package com.shreydh.startup.views;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

public class ViewIdea {
    private Long ideaId;
    private String body;
    private String title;
    private Long userId;
    private String mediaUrl;
    private int goalAmount;
    private int targetDays;
    private boolean isActive;


    public Long getIdeaId() {
        return ideaId;
    }

    public void setIdeaId(Long ideaId) {
        this.ideaId = ideaId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public int getGoalAmount() {
        return goalAmount;
    }

    public void setGoalAmount(int goalAmount) {
        this.goalAmount = goalAmount;
    }

    public int getTargetDays() {
        return targetDays;
    }

    public void setTargetDays(int targetDays) {
        this.targetDays = targetDays;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ViewIdea viewIdea = (ViewIdea) o;
        return goalAmount == viewIdea.goalAmount &&
                targetDays == viewIdea.targetDays &&
                isActive == viewIdea.isActive &&
                Objects.equals(ideaId, viewIdea.ideaId) &&
                Objects.equals(body, viewIdea.body) &&
                Objects.equals(title, viewIdea.title) &&
                Objects.equals(userId, viewIdea.userId) &&
                Objects.equals(mediaUrl, viewIdea.mediaUrl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(ideaId, body, title, userId, mediaUrl, goalAmount, targetDays, isActive);
    }
}
