package com.shreydh.startup.converters.impl;

import com.shreydh.startup.converters.IIdeaConverter;
import com.shreydh.startup.domains.DomainIdea;
import com.shreydh.startup.views.ViewIdea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
@Service
public class IdeaConverter implements IIdeaConverter {
    

    @Override
    public DomainIdea viewToDomain(ViewIdea viewIdea) {
        DomainIdea domainIdea = new DomainIdea();
        domainIdea.setIdeaId(viewIdea.getIdeaId());
        domainIdea.setTitle(viewIdea.getTitle());
        domainIdea.setBody(viewIdea.getBody());
        domainIdea.setUserId(viewIdea.getUserId());
        domainIdea.setGoalAmount(viewIdea.getGoalAmount());
        domainIdea.setTargetDays(viewIdea.getTargetDays());
        domainIdea.setIdeaActive(viewIdea.isActive());
        domainIdea.setMediaUrl(viewIdea.getMediaUrl());
        return domainIdea;

    }

    @Override
    public ViewIdea domainToView(DomainIdea domainIdea) {
        ViewIdea viewIdea = new ViewIdea();
        viewIdea.setIdeaId(domainIdea.getIdeaId());
        viewIdea.setTitle(domainIdea.getTitle());
        viewIdea.setBody(domainIdea.getBody());
        viewIdea.setUserId(domainIdea.getUserId());
        viewIdea.setGoalAmount(domainIdea.getGoalAmount());
        viewIdea.setTargetDays(domainIdea.getTargetDays());
        viewIdea.setActive(domainIdea.isIdeaActive());
        viewIdea.setMediaUrl(domainIdea.getMediaUrl());
        return viewIdea;

    }
   
}
