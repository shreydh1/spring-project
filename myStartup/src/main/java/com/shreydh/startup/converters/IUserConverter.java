package com.shreydh.startup.converters;

import com.shreydh.startup.domains.DomainIdea;
import com.shreydh.startup.domains.DomainUser;
import com.shreydh.startup.views.ViewIdea;
import com.shreydh.startup.views.ViewUser;

public interface IUserConverter {
    DomainUser viewToDomain(ViewUser viewUser);
    ViewUser domainToView(DomainUser domainUser);
}
