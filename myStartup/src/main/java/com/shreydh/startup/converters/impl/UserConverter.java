package com.shreydh.startup.converters.impl;

import com.shreydh.startup.converters.IUserConverter;
import com.shreydh.startup.domains.DomainIdea;
import com.shreydh.startup.domains.DomainUser;
import com.shreydh.startup.views.ViewIdea;
import com.shreydh.startup.views.ViewUser;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserConverter implements IUserConverter {

    public DomainUser viewToDomain(ViewUser viewUser){
        DomainUser domainUser = new DomainUser();
        domainUser.setUserId(viewUser.getUserId());
        domainUser.setEmail(viewUser.getEmail());
        domainUser.setName(viewUser.getName());
        domainUser.setPassword(viewUser.getPassword());
        domainUser.setActive(viewUser.isActive());

        return domainUser;
    }
    public ViewUser domainToView(DomainUser domainUser){
        ViewUser viewUser = new ViewUser();
        viewUser.setUserId(domainUser.getUserId());
        viewUser.setEmail(domainUser.getEmail());
        viewUser.setName(domainUser.getName());
        viewUser.setPassword(domainUser.getPassword());
        viewUser.setActive(domainUser.isActive());

        return viewUser;
    }
}
