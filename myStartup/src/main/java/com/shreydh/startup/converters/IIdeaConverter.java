package com.shreydh.startup.converters;

import com.shreydh.startup.domains.DomainIdea;
import com.shreydh.startup.views.ViewIdea;

public interface IIdeaConverter {


        DomainIdea viewToDomain(ViewIdea viewPost);
        ViewIdea domainToView(DomainIdea domainPost);
}
