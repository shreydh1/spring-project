package com.shreydh.startup;

import com.shreydh.startup.accessors.IIdeaAccessor;
import com.shreydh.startup.domains.DomainIdea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {


    public static void main(String[] args) throws Exception{
        SpringApplication.run(Application.class, args);
    }
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    @Bean
    public CommandLineRunner startup(IIdeaAccessor ideaAccessor) {
        return new CommandLineRunner() {
            @Override
            public void run(String... strings) throws Exception {
                DomainIdea post = new DomainIdea();
                post.setTitle("My new post");
                post.setBody("This is the body");
                post.setMediaUrl("www.youtube.com/funny");
                post.setUserId(1L);
                ideaAccessor.save(post);
                LOGGER.info("Created a new post: {}", post);

                LOGGER.info("All posts: " + ideaAccessor.findAllByUserId(1L));

//                LOGGER.info("More posts: " + ideaAccessor.findAll());
//
//                LOGGER.info("Author is Brian: " + (ideaAccessor.findAllByUserId(1L).toString()));
//

            }
        };
    }

}
