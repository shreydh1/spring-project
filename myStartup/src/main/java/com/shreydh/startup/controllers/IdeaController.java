package com.shreydh.startup.controllers;

import com.shreydh.startup.domains.DomainIdea;
import com.shreydh.startup.manager.impl.IdeaManager;
import com.shreydh.startup.views.ViewIdea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

@RestController
@RequestMapping("/api/v1/idea")
public class IdeaController {
    @Autowired
    IdeaManager ideaManager;


    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<List<ViewIdea>> getAllIdeas() {
        return new ResponseEntity<>(ideaManager.getAllIdeas(), HttpStatus.OK);
    }

    @RequestMapping(value="/{ideaId}", method = RequestMethod.GET)
    ResponseEntity<ViewIdea> getIdea(@PathVariable Long ideaId){
        return new ResponseEntity<>(ideaManager.getIdeaById(ideaId), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    ResponseEntity<ViewIdea> createIdea(@RequestBody ViewIdea viewIdea){
        return new ResponseEntity<>(ideaManager.createIdea(viewIdea), HttpStatus.OK);
    }

    @RequestMapping(value = "/{ideaId}", method = RequestMethod.PUT)
    ResponseEntity<ViewIdea> updateIdea(@PathVariable Long ideaId, @RequestBody ViewIdea viewIdea){
               return new ResponseEntity<>(ideaManager.updateIdea(ideaId, viewIdea), HttpStatus.OK);
           }
    @RequestMapping(value = "/{ideaId}", method = RequestMethod.DELETE)
    ResponseEntity<ViewIdea> deleteIdea(@PathVariable Long ideaId){
        return new ResponseEntity<>( ideaManager.deleteIdea(ideaId), HttpStatus.OK);
    }

}
