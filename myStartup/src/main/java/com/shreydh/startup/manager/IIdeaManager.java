package com.shreydh.startup.manager;

import com.shreydh.startup.views.ViewIdea;

import java.util.List;

public interface IIdeaManager {
    List<ViewIdea> getAllIdeas();


    ViewIdea getIdeaById(Long ideaId);


    ViewIdea createIdea(ViewIdea ViewIdea);


    ViewIdea updateIdea(Long postId, ViewIdea post);


    ViewIdea deleteIdea(Long ideaId);
}
