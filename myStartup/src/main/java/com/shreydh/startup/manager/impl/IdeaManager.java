package com.shreydh.startup.manager.impl;

import com.shreydh.startup.accessors.IIdeaAccessor;
import com.shreydh.startup.converters.IIdeaConverter;
import com.shreydh.startup.converters.impl.IdeaConverter;
import com.shreydh.startup.domains.DomainIdea;
import com.shreydh.startup.manager.IIdeaManager;
import com.shreydh.startup.views.ViewIdea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class IdeaManager implements IIdeaManager {
    @Autowired
    private IIdeaAccessor ideaAccessor;
    @Autowired
    private IIdeaConverter ideaConverter;

    IdeaManager(IIdeaAccessor ideaAccessor){
        this.ideaAccessor = ideaAccessor;
    }

    @Override
    public List<ViewIdea> getAllIdeas() {
        return ideaAccessor.findAll().stream()
                .map(ideaConverter::domainToView)
                .collect(Collectors.toList());
    }

    @Override
    public ViewIdea getIdeaById(Long ideaId) {
        DomainIdea domainIdea = ideaAccessor.findOne(ideaId);
        if (domainIdea == null)
            throw new EntityNotFoundException("Unable to retrieve idea: " + ideaId.toString());

            return ideaConverter.domainToView(domainIdea);
    }

    @Override
    public ViewIdea createIdea(ViewIdea viewIdea) {
        return ideaConverter.domainToView(ideaAccessor.save(ideaConverter.viewToDomain(viewIdea)));
    }

    @Override
    public ViewIdea updateIdea(Long ideaId, ViewIdea viewIdea) {
        DomainIdea currentIdea = ideaAccessor.findOne(ideaId);
        if (currentIdea == null) {
            throw new EntityNotFoundException("Unable to retrieve post: " + ideaId.toString());
        } else if (!currentIdea.getIdeaId().equals(ideaId)) {
            throw new InvalidParameterException("Provided idea id: " + ideaId + " does not match provided idea: " + currentIdea);
        }
        return ideaConverter.domainToView(ideaAccessor.save(ideaConverter.viewToDomain(viewIdea)));
    }

    @Override
    public ViewIdea deleteIdea(Long ideaId) {
    DomainIdea domainIdea = ideaAccessor.findOne(ideaId);
        if (domainIdea == null) {
            throw new EntityNotFoundException("Unable to retrieve idea: " + ideaId.toString());
        }

        ideaAccessor.delete(ideaId);
        return ideaConverter.domainToView(domainIdea);
    }

}
