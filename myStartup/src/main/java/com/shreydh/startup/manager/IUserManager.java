package com.shreydh.startup.manager;

import com.shreydh.startup.domains.DomainUser;
import com.shreydh.startup.views.ViewUser;

import java.util.List;

public interface IUserManager {
    List<ViewUser> getAllUsers();
    ViewUser getUserById(Long userId);
    ViewUser createUser(ViewUser viewUser);
    ViewUser updateUser(Long userId, ViewUser viewUser);
    ViewUser deleteUser(Long userId);
    ViewUser getUserByEmail(String email);
}
