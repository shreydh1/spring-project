package com.shreydh.startup.accessors;

import com.shreydh.startup.domains.DomainUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IUserAccessor extends JpaRepository<DomainUser, Long> {
    DomainUser findByEmail(String email);
    List<DomainUser> findAllByUserId(Long userId);
}
