package com.shreydh.startup.accessors;

import com.shreydh.startup.domains.DomainIdea;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IIdeaAccessor extends JpaRepository<DomainIdea, Long> {
    List<DomainIdea> findAllByUserId(Long user_id);

}
