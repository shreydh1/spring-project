package com.shreydh.startup.domains;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "users")
public class DomainUser {
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "is_Active")
    private boolean isActive;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainUser domainUser = (DomainUser) o;
        return isActive == domainUser.isActive &&
                Objects.equals(userId, domainUser.userId) &&
                Objects.equals(name, domainUser.name) &&
                Objects.equals(email, domainUser.email) &&
                Objects.equals(password, domainUser.password);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId,name, email, password, isActive);
    }

    @Override
    public String toString() {
        return "DomainUser{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
