package com.shreydh.startup.domains;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ideas")
public class DomainIdea {

    @Id
    @Column(name = "idea_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ideaId;

    @Column(name = "body")
    private String body;

    @Column(name = "title")
    private String title;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "media_url")
    private String mediaUrl;

    @Column(name = "goal_amount")
    private int goalAmount;

    @Column(name = "target_days")
    private int targetDays;

    @Column(name = "is_active")
    public boolean isActive;

    public boolean isIdeaActive() {
        return isActive;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setIdeaActive(boolean active) {
        isActive = active;
    }



    public Long getIdeaId() {

        return ideaId;
    }

    public void setIdeaId(Long ideaId) {
        this.ideaId = ideaId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public int getGoalAmount() {
        return goalAmount;
    }

    public void setGoalAmount(int goalAmount) {
        this.goalAmount = goalAmount;
    }

    public int getTargetDays() {
        return targetDays;
    }

    public void setTargetDays(int targetDays) {
        this.targetDays = targetDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainIdea domainIdea = (DomainIdea) o;
        return goalAmount == domainIdea.goalAmount &&
                targetDays == domainIdea.targetDays &&
                Objects.equals(ideaId, domainIdea.ideaId) &&
                Objects.equals(body, domainIdea.body) &&
                Objects.equals(title, domainIdea.title) &&
                Objects.equals(mediaUrl, domainIdea.mediaUrl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(ideaId, body, title, userId, mediaUrl, goalAmount, targetDays);
    }

    @Override
    public String toString() {
        return "DomainIdea{" +
                "ideaId=" + ideaId +
                ", body='" + body + '\'' +
                ", title='" + title + '\'' +
                ", userId=" + userId +
                ", mediaUrl='" + mediaUrl + '\'' +
                ", goalAmount=" + goalAmount +
                ", targetDays=" + targetDays +
                '}';
    }
}