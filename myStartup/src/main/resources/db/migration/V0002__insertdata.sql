INSERT INTO users (name, email, password, is_Active) VALUES
  ('Ram', 'ram@example.com', 'abcdef', TRUE ),
  ('Sam', 'sam@doe.com', 'sam1234', TRUE ),
  ('Hari', 'hari@ram.com', 'Hari1234', TRUE );


INSERT INTO ideas(title,body,media_url,goal_amount,target_days,is_Active,user_id) VALUES
  ('Pet-Share','This is a project where people will be able to find owners for the baby pets.','https://bit.ly/2I3MNAV',1000,20,TRUE ,1),
  ( 'Roll-Easy','Add wheels to your shoes easy.','https://bit.ly/2I26FII',2000,30,TRUE ,2),
  ('farm-Invest','Invest in farming and get fruity returns.','https://bit.ly/2rsHxAS',3000,20,TRUE, 3);

INSERT INTO funds(amount,isExist,user_id) VALUES
  (150, TRUE ,1),
  (100, false,2);