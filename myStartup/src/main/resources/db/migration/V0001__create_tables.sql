CREATE TABLE users (
  user_id BIGSERIAL PRIMARY KEY ,
  name TEXT NOT NULL,
  email TEXT NOT NULL,
  password TEXT NOT NULL,
  is_Active BOOLEAN
);
CREATE TABLE ideas (
  idea_id BIGSERIAL PRIMARY KEY,
  title TEXT NOT NULL,
  body TEXT NOT NULL,
  media_url TEXT,
  goal_amount INT NOT NULL ,
  target_days INT NOT NULL,
  is_active BOOLEAN ,
  user_id INT NOT NULL ,
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);
CREATE TABLE funds(
  fund_id BIGSERIAL PRIMARY KEY ,
  amount BIGINT,
  isExist BOOLEAN,
  user_id  INT NOT NULL ,
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);
